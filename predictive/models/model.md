�� sr 4com.rapidminer.operator.ports.metadata.ModelMetaData        L trainingSetMetaDatat ;Lcom/rapidminer/operator/ports/metadata/ExampleSetMetaData;xr /com.rapidminer.operator.ports.metadata.MetaData        L annotationst %Lcom/rapidminer/operator/Annotations;L 	dataClasst Ljava/lang/Class;L keyValueMapt Ljava/util/Map;xpsr #com.rapidminer.operator.Annotations        L keyValueMapt Ljava/util/LinkedHashMap;xpsr java.util.LinkedHashMap4�N\l�� Z accessOrderxr java.util.HashMap���`� F 
loadFactorI 	thresholdxp?@      w       x vr Bcom.rapidminer.operator.learner.tree.ConfigurableRandomForestModel        L modelt 7Lcom/rapidminer/operator/learner/SimplePredictionModel;xr 5com.rapidminer.operator.learner.SimplePredictionModelW|-(l�  xr /com.rapidminer.operator.learner.PredictionModelW]����#� L compareDataTypet CLcom/rapidminer/example/set/ExampleSetUtilities$TypesCompareOption;L compareSetSizet BLcom/rapidminer/example/set/ExampleSetUtilities$SetsCompareOption;xr %com.rapidminer.operator.AbstractModelð|ޡ5 Z showProgressL headerExampleSett -Lcom/rapidminer/example/set/HeaderExampleSet;L operatort "Lcom/rapidminer/operator/Operator;xr +com.rapidminer.operator.ResultObjectAdapter�X�
吗� L annotationsq ~ xr (com.rapidminer.operator.AbstractIOObjectb��8p>� L sourcet Ljava/lang/String;xpsq ~ ?@      w       xsr 9com.rapidminer.operator.ports.metadata.ExampleSetMetaData        Z nominalDataWasShrinkedL attributeMetaDataq ~ L attributesRelationt 4Lcom/rapidminer/operator/ports/metadata/SetRelation;L numberOfExamplest 2Lcom/rapidminer/operator/ports/metadata/MDInteger;xq ~ sq ~ sq ~ 
?@     w      t Sourcet�WITH
trans AS (SELECT * FROM trans_train), -- also change below
loan AS (SELECT * FROM loan_train),
card AS (SELECT * FROM card_train),
account_extra_info AS (
  SELECT
    *,
    (SELECT MIN(balance::REAL) FROM trans_train WHERE trans_train.account_id = x.account_id AND trans_train.date = latest_transaction_date) AS last_balance
  FROM (
    SELECT
      account_id,
      MAX(balance::REAL) AS max_balance,
      MIN(balance::REAL) AS min_balance,
      AVG(balance::REAL) AS avg_balance,
      abs(percentile_cont(0.5) WITHIN GROUP (ORDER BY balance::real) / percentile_cont(0.1) WITHIN GROUP (ORDER BY balance::real)) AS balance_drop_ratio,
      MAX(date) AS latest_transaction_date,
      bool_or(operation = 'remittance to another bank') AS has_remittance,
      COUNT(DISTINCT bank) FILTER (WHERE bank <> '') AS bank_connections
    FROM trans
    GROUP BY (account_id)
  ) AS x
),trans_info_month AS (
  SELECT
    account_id,
    COALESCE(SUM(amount::REAL) FILTER (WHERE type = 'credit' AND k_symbol <> 'interest credited'), 0) AS credit,
    COALESCE(SUM(amount::REAL) FILTER (WHERE type = 'credit' AND k_symbol = 'interest credited'), 0) AS interest_credit,
    COALESCE(SUM(amount::REAL) FILTER (WHERE type = 'withdrawal' OR type = 'withdrawal in cash'), 0) AS widthdrawal,
    COALESCE(COUNT(*)) AS trans_count
  FROM trans
  GROUP BY (account_id, substring(date FROM 1 FOR 4))
), trans_info AS (
  SELECT
    account_id,
    AVG(credit) AS avg_month_income,
    AVG(interest_credit) AS avg_month_interest_credit,
    AVG(widthdrawal) AS avg_month_widthdrawal,
    AVG(trans_count) AS avg_month_trans_count,
    stddev_pop(credit) AS stddev_credit
    FROM trans_info_month
    GROUP BY (account_id)
), clients_per_account AS (
  SELECT
    account_id,
    COUNT(client_id) AS clients_count,
    SUM(avg_month_income) account_sum_avg_income
  FROM disp
  LEFT JOIN trans_info USING (account_id)
  GROUP BY (account_id)
)
SELECT
  l.loan_id,
  --to_date(l.date, 'YYMMDD') AS date,
  l.amount::int,
  l.duration::int,
  l.payments::int,
  a.frequency AS account_frequency,
  a.district_id,
  --to_date(a.date, 'YYMMDD') AS account_date,
  to_date(l.date, 'YYMMDD') - to_date(a.date, 'YYMMDD') AS account_age,
  d.region AS owner_region,
  dist_account.region as account_region,

  dist_account.no__of_inhabitants::int,
  dist_account.no__of_municipalities_with_inhabitants_less_499::int,
  dist_account.no__of_municipalities_with_inhabitants_500_1999::int,
  dist_account.no__of_municipalities_with_inhabitants_2000_9999::int,
  dist_account.no__of_municipalities_with_inhabitants_greater_10000::int,
  dist_account.no__of_cities::int,
  dist_account.ratio_of_urban_inhabitants::real,
  dist_account.average_salary::int,
  dist_account.unemploymant_rate_96::real,
  dist_account.no__of_enterpreneurs_per_1000_inhabitants::int,
  dist_account.no__of_commited_crimes_96::int,

  d.no__of_inhabitants::int AS owner_no__of_inhabitants,
  d.no__of_municipalities_with_inhabitants_less_499::int AS owner_no__of_municipalities_with_inhabitants_less_499,
  d.no__of_municipalities_with_inhabitants_500_1999::int AS owner_no__of_municipalities_with_inhabitants_500_1999,
  d.no__of_municipalities_with_inhabitants_2000_9999::int AS owner_no__of_municipalities_with_inhabitants_2000_9999,
  d.no__of_municipalities_with_inhabitants_greater_10000::int AS owner_no__of_municipalities_with_inhabitants_greater_10000,
  d.no__of_cities::int AS owner_no__of_cities,
  d.ratio_of_urban_inhabitants::real AS owner_ratio_of_urban_inhabitants,
  d.average_salary::int AS owner_average_salary,
  d.unemploymant_rate_96::real AS owner_unemploymant_rate_96,
  d.no__of_enterpreneurs_per_1000_inhabitants::int AS owner_no__of_enterpreneurs_per_1000_inhabitants,
  d.no__of_commited_crimes_96::int AS owner_no__of_commited_crimes_96,

  CASE WHEN substring(co.birth_number FROM 3 FOR 2)::int >= 50 THEN 'female' ELSE 'male' END AS owner_sex,
  date_part('year', age(to_date(l.date, 'YYMMDD'), client_birthday(co.birth_number))) AS owner_age,
  ca.clients_count AS clients_count,
  c.type AS card_type,
  c.type IS NOT NULL AS has_credit_card,
  date_part('year', age(to_date(c.issued, 'YYMMDD'), client_birthday(co.birth_number))) AS card_issued_owner_age,
  date_part('year', age(to_date(c.issued, 'YYMMDD'), to_date(a.date, 'YYMMDD'))) AS card_issued_account_age,
  to_date(l.date, 'YYMMDD') - to_date(c.issued, 'YYMMDD') AS loan_to_card_issued_diff,
  trans_info.avg_month_income,
  trans_info.avg_month_interest_credit,
  trans_info.avg_month_widthdrawal,
  trans_info.avg_month_trans_count,
  trans_info.stddev_credit / trans_info.avg_month_income AS credit_instability,
  --account_extra_info.max_balance,
  --account_extra_info.min_balance,
  account_extra_info.min_balance < 0 AS has_had_negative_balance,
  --account_extra_info.avg_balance,
  account_extra_info.balance_drop_ratio,
  account_extra_info.has_remittance,
  account_extra_info.bank_connections,
  trans_info.avg_month_widthdrawal / trans_info.avg_month_income AS percentage_spent,

  trans_info.avg_month_income / ((dist_account.average_salary::int + d.average_salary::int) / 2) AS salary_comparisson,
  --account_extra_info.avg_balance / (dist_account.average_salary::int + d.average_salary::int) / 2 AS salary_comparisson2,
  --account_extra_info.max_balance / dist_account.average_salary::int AS salary_comparisson3,
  --trans_info.avg_month_interest_credit / trans_info.avg_month_income AS interest_credit_percentage,
  --l.amount::int / dist_account.average_salary::int AS amount_avg_salary_ratio,
  -- TODO: amount per sum of salary of all account clients
  --l.amount::int / trans_info.avg_month_income AS amount_avg_credit_ratio,

  l.payments::int / ca.account_sum_avg_income AS shared_percentage_payment,
  l.payments::int / trans_info.avg_month_income AS percentage_payment,

  last_balance / account_extra_info.max_balance AS percentage_from_max_balance,

  d.code <> dist_account.code AS bank_in_other_district,

  l.status
FROM loan AS l
LEFT JOIN account AS a USING (account_id)
LEFT JOIN trans_info USING (account_id)
LEFT JOIN account_extra_info USING (account_id)
LEFT JOIN district AS dist_account ON (a.district_id = dist_account.code)
LEFT JOIN disp AS disp_owner ON (disp_owner.type = 'OWNER' AND disp_owner.account_id = a.account_id)
LEFT JOIN client AS co ON (co.client_id = disp_owner.client_id)
LEFT JOIN district AS d ON (co.district_id = d.code)
LEFT JOIN clients_per_account ca ON (a.account_id = ca.account_id)
LEFT JOIN card AS c ON (disp_owner.disp_id = c.disp_id);x vr !com.rapidminer.example.ExampleSet8�i�WNp�  xpsq ~ ?@      w       x sq ~ 
?@     `w   �   7t amountsr 8com.rapidminer.operator.ports.metadata.AttributeMetaData        I typeL annotationsq ~ L meant /Lcom/rapidminer/operator/ports/metadata/MDReal;L modeq ~ L nameq ~ L numberOfMissingValuesq ~ L ownerq ~ L roleq ~ L 
valueRanget +Lcom/rapidminer/tools/math/container/Range;L valueSett Ljava/util/Set;L valueSetRelationq ~ xp   sq ~ sq ~ 
?@      w       x sr -com.rapidminer.operator.ports.metadata.MDReal         xr /com.rapidminer.operator.ports.metadata.MDNumber        L numbert Ljava/lang/Number;L relationt :Lcom/rapidminer/operator/ports/metadata/MDNumber$Relation;xpsr java.lang.Double���J)k� D valuexr java.lang.Number������  xp�      ~r 8com.rapidminer.operator.ports.metadata.MDNumber$Relation          xr java.lang.Enum          xpt EQUALpq ~ 'sr 0com.rapidminer.operator.ports.metadata.MDInteger         xq ~ 0sr java.lang.Integer⠤���8 I valuexq ~ 5    q ~ 9q ~ psr )com.rapidminer.tools.math.container.Range        D lowerD upperxp�      �      sr java.util.TreeSetݘP���[  xppw    x~r 2com.rapidminer.operator.ports.metadata.SetRelation          xq ~ 8t EQUALt durationsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~ Fsq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt paymentssq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~ Osq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt account_frequencysq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /p~q ~ 7t UNKNOWNpq ~ Xsq ~ ;q ~ >q ~ 9q ~ psq ~ ?��      �      sq ~ Apw   t issuance after transactiont monthly issuancet weekly issuancexq ~ Dt district_idsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /pq ~ ]pq ~ esq ~ ;q ~ >q ~ 9q ~ psq ~ ?��      �      sq ~ Apw   It 1t 10t 11t 12t 13t 14t 15t 16t 17t 18t 19t 2t 20t 21t 23t 24t 25t 26t 27t 28t 29t 3t 30t 31t 32t 33t 34t 37t 38t 39t 4t 40t 41t 42t 43t 44t 45t 46t 47t 48t 49t 5t 50t 51t 52t 53t 54t 55t 56t 57t 58t 59t 6t 60t 61t 62t 63t 64t 65t 66t 67t 68t 69t 7t 70t 71t 72t 73t 74t 75t 77t 8t 9xq ~ Dt account_agesq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~ �sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt owner_regionsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /pq ~ ]pq ~ �sq ~ ;q ~ >q ~ 9q ~ psq ~ ?��      �      sq ~ Apw   t Praguet central Bohemiat east Bohemiat north Bohemiat north Moraviat south Bohemiat south Moraviat west Bohemiaxq ~ Dt account_regionsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /pq ~ ]pq ~ �sq ~ ;q ~ >q ~ 9q ~ psq ~ ?��      �      sq ~ Apw   t Praguet central Bohemiat east Bohemiat north Bohemiat north Moraviat south Bohemiat south Moraviat west Bohemiaxq ~ Dt no__of_inhabitantssq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~ �sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt /no__of_municipalities_with_inhabitants_less_499sq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~ �sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt /no__of_municipalities_with_inhabitants_500_1999sq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~ �sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt 0no__of_municipalities_with_inhabitants_2000_9999sq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~ �sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt 4no__of_municipalities_with_inhabitants_greater_10000sq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt no__of_citiessq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt ratio_of_urban_inhabitantssq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt average_salarysq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt unemploymant_rate_96sq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~'sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt )no__of_enterpreneurs_per_1000_inhabitantssq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~0sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt no__of_commited_crimes_96sq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~9sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt owner_no__of_inhabitantssq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~Bsq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt 5owner_no__of_municipalities_with_inhabitants_less_499sq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~Ksq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt 5owner_no__of_municipalities_with_inhabitants_500_1999sq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~Tsq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt 6owner_no__of_municipalities_with_inhabitants_2000_9999sq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~]sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt :owner_no__of_municipalities_with_inhabitants_greater_10000sq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~fsq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt owner_no__of_citiessq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~osq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt  owner_ratio_of_urban_inhabitantssq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~xsq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt owner_average_salarysq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt owner_unemploymant_rate_96sq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt /owner_no__of_enterpreneurs_per_1000_inhabitantssq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt owner_no__of_commited_crimes_96sq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt 	owner_sexsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /pq ~ ]pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?��      �      sq ~ Apw   t femalet malexq ~ Dt 	owner_agesq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt clients_countsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt 	card_typesq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /pq ~ ]pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?��      �      sq ~ Apw   t classict goldt juniorxq ~ Dt has_credit_cardsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /pq ~ ]pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?��      �      sq ~ Apw   t ft txq ~ Dt card_issued_owner_agesq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt card_issued_account_agesq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt loan_to_card_issued_diffsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt avg_month_incomesq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt avg_month_interest_creditsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~�sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt avg_month_widthdrawalsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt avg_month_trans_countsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt credit_instabilitysq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt has_had_negative_balancesq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /pq ~ ]pq ~sq ~ ;q ~ >q ~ 9q ~ psq ~ ?��      �      sq ~ Apw   t ft txq ~ Dt balance_drop_ratiosq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~(sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt has_remittancesq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /pq ~ ]pq ~1sq ~ ;q ~ >q ~ 9q ~ psq ~ ?��      �      sq ~ Apw   t ft txq ~ Dt bank_connectionssq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~;sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt percentage_spentsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~Dsq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt salary_comparissonsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~Msq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt shared_percentage_paymentsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~Vsq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt percentage_paymentsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~_sq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt percentage_from_max_balancesq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /sq ~ 4�      q ~ 9pq ~hsq ~ ;q ~ >q ~ 9q ~ psq ~ ?�      �      sq ~ Apw    xq ~ Dt bank_in_other_districtsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /pq ~ ]pq ~qsq ~ ;q ~ >q ~ 9q ~ psq ~ ?��      �      sq ~ Apw   t ft txq ~ Dt loan_idsq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /pq ~ ]pq ~{sq ~ ;q ~ >q ~ 9q ~ t idsq ~ ?��      �      sq ~ Apw  Ht 4959t 4961t 4973t 4996t 5002t 5032t 5044t 5045t 5060t 5082t 5088t 5125t 5126t 5128t 5130t 5131t 5132t 5136t 5138t 5145t 5148t 5151t 5160t 5161t 5170t 5176t 5189t 5208t 5209t 5212t 5237t 5269t 5282t 5285t 5301t 5310t 5311t 5312t 5314t 5316t 5319t 5325t 5329t 5337t 5350t 5351t 5352t 5366t 5375t 5385t 5388t 5390t 5395t 5409t 5413t 5414t 5415t 5418t 5429t 5445t 5449t 5450t 5452t 5453t 5464t 5470t 5479t 5482t 5494t 5495t 5519t 5523t 5533t 5549t 5563t 5568t 5576t 5586t 5591t 5593t 5595t 5599t 5606t 5610t 5611t 5624t 5625t 5635t 5657t 5693t 5706t 5713t 5723t 5724t 5725t 5739t 5740t 5741t 5750t 5754t 5804t 5805t 5836t 5837t 5861t 5876t 5877t 5880t 5882t 5886t 5887t 5892t 5897t 5900t 5918t 5922t 5923t 5938t 5959t 5970t 5980t 5988t 5989t 5997t 5999t 6004t 6006t 6011t 6012t 6013t 6015t 6019t 6027t 6038t 6043t 6063t 6064t 6066t 6072t 6073t 6077t 6087t 6103t 6109t 6111t 6112t 6120t 6130t 6137t 6144t 6150t 6151t 6153t 6157t 6169t 6175t 6178t 6185t 6190t 6202t 6210t 6216t 6223t 6227t 6228t 6229t 6232t 6244t 6253t 6272t 6279t 6283t 6294t 6296t 6297t 6302t 6303t 6306t 6309t 6315t 6316t 6336t 6342t 6354t 6356t 6358t 6362t 6403t 6407t 6421t 6456t 6460t 6461t 6471t 6474t 6499t 6501t 6509t 6512t 6520t 6526t 6535t 6540t 6545t 6546t 6550t 6577t 6578t 6580t 6585t 6589t 6591t 6594t 6596t 6598t 6599t 6600t 6613t 6624t 6642t 6644t 6645t 6647t 6650t 6659t 6667t 6668t 6676t 6678t 6686t 6687t 6694t 6696t 6698t 6699t 6704t 6712t 6715t 6721t 6725t 6726t 6729t 6732t 6734t 6736t 6737t 6738t 6745t 6751t 6764t 6785t 6805t 6808t 6810t 6816t 6817t 6818t 6820t 6836t 6841t 6863t 6865t 6876t 6888t 6895t 6903t 6919t 6921t 6929t 6933t 6940t 6944t 6949t 6950t 6955t 6960t 6961t 6965t 6985t 6986t 6992t 6995t 6996t 6998t 7004t 7008t 7013t 7034t 7036t 7055t 7057t 7061t 7066t 7097t 7101t 7104t 7121t 7130t 7136t 7137t 7138t 7142t 7154t 7166t 7176t 7189t 7192t 7194t 7198t 7213t 7220t 7226t 7227t 7233t 7235t 7240t 7241t 7243t 7246t 7249t 7259t 7262t 7263t 7271t 7284t 7304t 7305t 7308xq ~ Dt statussq ~ (   sq ~ sq ~ 
?@      w       x sq ~ /pq ~ ]pq ~�sq ~ ;q ~ >q ~ 9q ~ t labelsq ~ ?��      �      sq ~ Apw   t -1t 1xq ~ Dx q ~ Dsq ~ ;q ~ >q ~ 9