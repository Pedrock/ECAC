�� sr 9com.rapidminer.operator.ports.metadata.ExampleSetMetaData        Z nominalDataWasShrinkedL attributeMetaDatat Ljava/util/Map;L attributesRelationt 4Lcom/rapidminer/operator/ports/metadata/SetRelation;L numberOfExamplest 2Lcom/rapidminer/operator/ports/metadata/MDInteger;xr /com.rapidminer.operator.ports.metadata.MetaData        L annotationst %Lcom/rapidminer/operator/Annotations;L 	dataClasst Ljava/lang/Class;L keyValueMapq ~ xpsr #com.rapidminer.operator.Annotations        L keyValueMapt Ljava/util/LinkedHashMap;xpsr java.util.LinkedHashMap4�N\l�� Z accessOrderxr java.util.HashMap���`� F 
loadFactorI 	thresholdxp?@     w      t Sourcet�WITH
trans AS (SELECT * FROM trans_test), -- also change below
loan AS (SELECT * FROM loan_test),
card AS (SELECT * FROM card_test),
account_extra_info AS (
  SELECT
    *,
    (SELECT MIN(balance::REAL) FROM trans_test WHERE trans_test.account_id = x.account_id AND trans_test.date = latest_transaction_date) AS last_balance
  FROM (
    SELECT
      account_id,
      MAX(balance::REAL) AS max_balance,
      MIN(balance::REAL) AS min_balance,
      AVG(balance::REAL) AS avg_balance,
      abs(percentile_cont(0.5) WITHIN GROUP (ORDER BY balance::real) / percentile_cont(0.1) WITHIN GROUP (ORDER BY balance::real)) AS balance_drop_ratio,
      MAX(date) AS latest_transaction_date,
      bool_or(operation = 'remittance to another bank') AS has_remittance,
      COUNT(DISTINCT bank) FILTER (WHERE bank <> '') AS bank_connections
    FROM trans
    GROUP BY (account_id)
  ) AS x
),trans_info_month AS (
  SELECT
    account_id,
    COALESCE(SUM(amount::REAL) FILTER (WHERE type = 'credit' AND k_symbol <> 'interest credited'), 0) AS credit,
    COALESCE(SUM(amount::REAL) FILTER (WHERE type = 'credit' AND k_symbol = 'interest credited'), 0) AS interest_credit,
    COALESCE(SUM(amount::REAL) FILTER (WHERE type = 'withdrawal' OR type = 'withdrawal in cash'), 0) AS widthdrawal,
    COALESCE(COUNT(*)) AS trans_count
  FROM trans
  GROUP BY (account_id, substring(date FROM 1 FOR 4))
), trans_info AS (
  SELECT
    account_id,
    AVG(credit) AS avg_month_income,
    AVG(interest_credit) AS avg_month_interest_credit,
    AVG(widthdrawal) AS avg_month_widthdrawal,
    AVG(trans_count) AS avg_month_trans_count,
    stddev_pop(credit) AS stddev_credit
    FROM trans_info_month
    GROUP BY (account_id)
), clients_per_account AS (
  SELECT
    account_id,
    COUNT(client_id) AS clients_count,
    SUM(avg_month_income) account_sum_avg_income
  FROM disp
  LEFT JOIN trans_info USING (account_id)
  GROUP BY (account_id)
)
SELECT
  l.loan_id,
  --to_date(l.date, 'YYMMDD') AS date,
  l.amount::int,
  l.duration::int,
  l.payments::int,
  a.frequency AS account_frequency,
  a.district_id,
  --to_date(a.date, 'YYMMDD') AS account_date,
  to_date(l.date, 'YYMMDD') - to_date(a.date, 'YYMMDD') AS account_age,
  d.region AS owner_region,
  dist_account.region as account_region,

  dist_account.no__of_inhabitants::int,
  dist_account.no__of_municipalities_with_inhabitants_less_499::int,
  dist_account.no__of_municipalities_with_inhabitants_500_1999::int,
  dist_account.no__of_municipalities_with_inhabitants_2000_9999::int,
  dist_account.no__of_municipalities_with_inhabitants_greater_10000::int,
  dist_account.no__of_cities::int,
  dist_account.ratio_of_urban_inhabitants::real,
  dist_account.average_salary::int,
  dist_account.unemploymant_rate_96::real,
  dist_account.no__of_enterpreneurs_per_1000_inhabitants::int,
  dist_account.no__of_commited_crimes_96::int,

  d.no__of_inhabitants::int AS owner_no__of_inhabitants,
  d.no__of_municipalities_with_inhabitants_less_499::int AS owner_no__of_municipalities_with_inhabitants_less_499,
  d.no__of_municipalities_with_inhabitants_500_1999::int AS owner_no__of_municipalities_with_inhabitants_500_1999,
  d.no__of_municipalities_with_inhabitants_2000_9999::int AS owner_no__of_municipalities_with_inhabitants_2000_9999,
  d.no__of_municipalities_with_inhabitants_greater_10000::int AS owner_no__of_municipalities_with_inhabitants_greater_10000,
  d.no__of_cities::int AS owner_no__of_cities,
  d.ratio_of_urban_inhabitants::real AS owner_ratio_of_urban_inhabitants,
  d.average_salary::int AS owner_average_salary,
  d.unemploymant_rate_96::real AS owner_unemploymant_rate_96,
  d.no__of_enterpreneurs_per_1000_inhabitants::int AS owner_no__of_enterpreneurs_per_1000_inhabitants,
  d.no__of_commited_crimes_96::int AS owner_no__of_commited_crimes_96,

  CASE WHEN substring(co.birth_number FROM 3 FOR 2)::int >= 50 THEN 'female' ELSE 'male' END AS owner_sex,
  date_part('year', age(to_date(l.date, 'YYMMDD'), client_birthday(co.birth_number))) AS owner_age,
  ca.clients_count AS clients_count,
  c.type AS card_type,
  c.type IS NOT NULL AS has_credit_card,
  date_part('year', age(to_date(c.issued, 'YYMMDD'), client_birthday(co.birth_number))) AS card_issued_owner_age,
  date_part('year', age(to_date(c.issued, 'YYMMDD'), to_date(a.date, 'YYMMDD'))) AS card_issued_account_age,
  to_date(l.date, 'YYMMDD') - to_date(c.issued, 'YYMMDD') AS loan_to_card_issued_diff,
  trans_info.avg_month_income,
  trans_info.avg_month_interest_credit,
  trans_info.avg_month_widthdrawal,
  trans_info.avg_month_trans_count,
  trans_info.stddev_credit / trans_info.avg_month_income AS credit_instability,
  account_extra_info.max_balance,
  --account_extra_info.min_balance,
  account_extra_info.min_balance < 0 AS has_had_negative_balance,
  --account_extra_info.avg_balance,
  account_extra_info.balance_drop_ratio,
  account_extra_info.has_remittance,
  account_extra_info.bank_connections,
  trans_info.avg_month_widthdrawal / trans_info.avg_month_income AS percentage_spent,

  trans_info.avg_month_income / ((dist_account.average_salary::int + d.average_salary::int) / 2) AS salary_comparisson,
  --account_extra_info.avg_balance / (dist_account.average_salary::int + d.average_salary::int) / 2 AS salary_comparisson2,
  --account_extra_info.max_balance / dist_account.average_salary::int AS salary_comparisson3,
  --trans_info.avg_month_interest_credit / trans_info.avg_month_income AS interest_credit_percentage,
  --l.amount::int / dist_account.average_salary::int AS amount_avg_salary_ratio,
  -- TODO: amount per sum of salary of all account clients
  --l.amount::int / trans_info.avg_month_income AS amount_avg_credit_ratio,

  l.payments::int / ca.account_sum_avg_income AS shared_percentage_payment,
  l.payments::int / trans_info.avg_month_income AS percentage_payment,

  last_balance / account_extra_info.max_balance AS percentage_from_max_balance,

  d.code <> dist_account.code AS bank_in_other_district,

  l.status
FROM loan AS l
LEFT JOIN account AS a USING (account_id)
LEFT JOIN trans_info USING (account_id)
LEFT JOIN account_extra_info USING (account_id)
LEFT JOIN district AS dist_account ON (a.district_id = dist_account.code)
LEFT JOIN disp AS disp_owner ON (disp_owner.type = 'OWNER' AND disp_owner.account_id = a.account_id)
LEFT JOIN client AS co ON (co.client_id = disp_owner.client_id)
LEFT JOIN district AS d ON (co.district_id = d.code)
LEFT JOIN clients_per_account ca ON (a.account_id = ca.account_id)
LEFT JOIN card AS c ON (disp_owner.disp_id = c.disp_id);x vr !com.rapidminer.example.ExampleSet8�i�WNp�  xpsq ~ ?@      w       x sq ~ ?@     `w   �   8t loan_idsr 8com.rapidminer.operator.ports.metadata.AttributeMetaData        I typeL annotationsq ~ L meant /Lcom/rapidminer/operator/ports/metadata/MDReal;L modet Ljava/lang/String;L nameq ~ L numberOfMissingValuesq ~ L ownert ;Lcom/rapidminer/operator/ports/metadata/ExampleSetMetaData;L roleq ~ L 
valueRanget +Lcom/rapidminer/tools/math/container/Range;L valueSett Ljava/util/Set;L valueSetRelationq ~ xp   sq ~ sq ~ ?@     w      t sql_typet textx sr -com.rapidminer.operator.ports.metadata.MDReal         xr /com.rapidminer.operator.ports.metadata.MDNumber        L numbert Ljava/lang/Number;L relationt :Lcom/rapidminer/operator/ports/metadata/MDNumber$Relation;xpp~r 8com.rapidminer.operator.ports.metadata.MDNumber$Relation          xr java.lang.Enum          xpt UNKNOWNt 6569q ~ sr 0com.rapidminer.operator.ports.metadata.MDInteger         xq ~ !sr java.lang.Integer⠤���8 I valuexr java.lang.Number������  xp    ~q ~ %t EQUALq ~ psr )com.rapidminer.tools.math.container.Range        D lowerD upperxp��      �      sr java.util.TreeSetݘP���[  xppw  bt 4962t 4967t 4968t 4986t 4988t 4989t 4990t 5005t 5015t 5027t 5034t 5036t 5039t 5041t 5042t 5043t 5046t 5051t 5061t 5063t 5072t 5086t 5103t 5110t 5117t 5124t 5134t 5140t 5147t 5154t 5158t 5169t 5172t 5174t 5206t 5207t 5210t 5214t 5221t 5226t 5229t 5236t 5241t 5245t 5253t 5260t 5265t 5270t 5281t 5287t 5293t 5304t 5313t 5318t 5320t 5332t 5334t 5338t 5343t 5346t 5354t 5358t 5360t 5363t 5365t 5367t 5368t 5369t 5377t 5382t 5384t 5389t 5398t 5419t 5420t 5428t 5435t 5446t 5447t 5451t 5463t 5469t 5486t 5491t 5501t 5520t 5525t 5526t 5527t 5531t 5532t 5556t 5561t 5569t 5582t 5584t 5585t 5589t 5614t 5616t 5620t 5634t 5638t 5643t 5644t 5651t 5656t 5665t 5669t 5674t 5681t 5682t 5683t 5698t 5699t 5718t 5719t 5731t 5758t 5760t 5765t 5772t 5773t 5774t 5779t 5782t 5784t 5790t 5799t 5808t 5815t 5817t 5823t 5830t 5841t 5850t 5856t 5862t 5865t 5868t 5878t 5889t 5893t 5895t 5906t 5909t 5930t 5931t 5932t 5933t 5940t 5942t 5953t 5956t 5963t 5973t 5976t 5977t 5991t 5992t 6007t 6010t 6014t 6028t 6037t 6044t 6052t 6054t 6055t 6059t 6075t 6076t 6078t 6083t 6088t 6095t 6097t 6104t 6113t 6118t 6136t 6142t 6147t 6168t 6173t 6196t 6207t 6215t 6234t 6235t 6237t 6239t 6242t 6249t 6254t 6255t 6278t 6310t 6312t 6313t 6320t 6321t 6322t 6338t 6346t 6350t 6353t 6355t 6357t 6377t 6386t 6399t 6402t 6415t 6435t 6436t 6437t 6442t 6450t 6464t 6468t 6469t 6470t 6486t 6490t 6495t 6502t 6508t 6534t 6539t 6541t 6544t 6552t 6553t 6554t 6562t 6568q ~ )t 6571t 6574t 6605t 6607t 6612t 6621t 6625t 6632t 6633t 6641t 6643t 6655t 6660t 6665t 6675t 6677t 6682t 6688t 6695t 6705t 6727t 6730t 6746t 6748t 6753t 6755t 6758t 6763t 6766t 6790t 6791t 6793t 6819t 6821t 6824t 6851t 6852t 6855t 6856t 6857t 6859t 6861t 6866t 6868t 6871t 6872t 6875t 6877t 6878t 6887t 6908t 6913t 6922t 6923t 6926t 6932t 6934t 6938t 6942t 6948t 6956t 6959t 6962t 6969t 6970t 6984t 7001t 7035t 7037t 7046t 7049t 7050t 7051t 7067t 7072t 7085t 7087t 7088t 7091t 7096t 7100t 7110t 7115t 7119t 7122t 7129t 7139t 7147t 7151t 7164t 7167t 7168t 7171t 7172t 7181t 7187t 7195t 7199t 7200t 7201t 7202t 7209t 7219t 7221t 7230t 7242t 7250t 7253t 7255t 7264t 7277t 7279t 7286t 7292t 7294t 7295x~r 2com.rapidminer.operator.ports.metadata.SetRelation          xq ~ &t EQUALt amountsq ~    sq ~ sq ~ ?@     w      q ~ t int4x sq ~  sr java.lang.Double���J)k� D valuexq ~ -A,<��Nq ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1@�     A"�    sq ~ 3pw    xq ~�t durationsq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@B�����8q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1@(      @N      sq ~ 3pw    xq ~�t paymentssq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@��z6��.q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1@s      @�[     sq ~ 3pw    xq ~�t account_frequencysq ~    sq ~ sq ~ ?@     w      q ~ q ~ x sq ~  pq ~ 't monthly issuanceq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1��      �      sq ~ 3pw   t issuance after transactionq ~�t weekly issuancexq ~�t district_idsq ~    sq ~ sq ~ ?@     w      q ~ q ~ x sq ~  pq ~ 't 1q ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1��      �      sq ~ 3pw   Kq ~�t 10t 11t 12t 13t 14t 15t 16t 17t 18t 19t 2t 20t 21t 22t 23t 24t 25t 26t 27t 28t 29t 3t 32t 33t 34t 35t 36t 37t 38t 39t 4t 40t 41t 42t 43t 44t 45t 46t 47t 48t 49t 5t 50t 51t 52t 53t 54t 55t 56t 57t 58t 59t 6t 60t 61t 62t 63t 64t 65t 66t 67t 68t 69t 7t 70t 71t 72t 73t 74t 75t 76t 77t 8t 9xq ~�t account_agesq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@zJ�����q ~ /pq ~sq ~ *q ~ .q ~ /q ~ psq ~ 1@Y�     @��     sq ~ 3pw    xq ~�t owner_regionsq ~    sq ~ sq ~ ?@     w      q ~ q ~ x sq ~  pq ~ 't north Moraviaq ~sq ~ *q ~ .q ~ /q ~ psq ~ 1��      �      sq ~ 3pw   t Praguet central Bohemiat east Bohemiat north Bohemiaq ~"t south Bohemiat south Moraviat west Bohemiaxq ~�t account_regionsq ~    sq ~ sq ~ ?@     w      q ~ q ~ x sq ~  pq ~ 't north Moraviaq ~-sq ~ *q ~ .q ~ /q ~ psq ~ 1��      �      sq ~ 3pw   t Praguet central Bohemiat east Bohemiat north Bohemiaq ~2t south Bohemiat south Moraviat west Bohemiaxq ~�t no__of_inhabitantssq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�AOZѷ�q ~ /pq ~=sq ~ *q ~ .q ~ /q ~ psq ~ 1@��    A2b�    sq ~ 3pw    xq ~�t /no__of_municipalities_with_inhabitants_less_499sq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@D	f�s��q ~ /pq ~Fsq ~ *q ~ .q ~ /q ~ psq ~ 1        @b�     sq ~ 3pw    xq ~�t /no__of_municipalities_with_inhabitants_500_1999sq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@5m2��kq ~ /pq ~Osq ~ *q ~ .q ~ /q ~ psq ~ 1        @Q�     sq ~ 3pw    xq ~�t 0no__of_municipalities_with_inhabitants_2000_9999sq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@c���q ~ /pq ~Xsq ~ *q ~ .q ~ /q ~ psq ~ 1        @4      sq ~ 3pw    xq ~�t 4no__of_municipalities_with_inhabitants_greater_10000sq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�?�VǗ�I�q ~ /pq ~asq ~ *q ~ .q ~ /q ~ psq ~ 1        @      sq ~ 3pw    xq ~�t no__of_citiessq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@P���xq ~ /pq ~jsq ~ *q ~ .q ~ /q ~ psq ~ 1?�      @&      sq ~ 3pw    xq ~�t ratio_of_urban_inhabitantssq ~    sq ~ sq ~ ?@     w      q ~ t float4x sq ~  sq ~�@P�źHq ~ /pq ~ssq ~ *q ~ .q ~ /q ~ psq ~ 1@@�3?�l�@Y      sq ~ 3pw    xq ~�t average_salarysq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@�e��<��q ~ /pq ~}sq ~ *q ~ .q ~ /q ~ psq ~ 1@��     @�~�    sq ~ 3pw    xq ~�t unemploymant_rate_96sq ~    sq ~ sq ~ ?@     w      q ~ q ~wx sq ~  sq ~�@��v#x�q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1?ۅ���@"�����sq ~ 3pw    xq ~�t )no__of_enterpreneurs_per_1000_inhabitantssq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@]��kq ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1@T@     @d�     sq ~ 3pw    xq ~�t no__of_commited_crimes_96sq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@�%���q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1@��     @�20    sq ~ 3pw    xq ~�t owner_no__of_inhabitantssq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�AOZѷ�q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1@��    A2b�    sq ~ 3pw    xq ~�t 5owner_no__of_municipalities_with_inhabitants_less_499sq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@D	f�s��q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1        @b�     sq ~ 3pw    xq ~�t 5owner_no__of_municipalities_with_inhabitants_500_1999sq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@5m2��kq ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1        @Q�     sq ~ 3pw    xq ~�t 6owner_no__of_municipalities_with_inhabitants_2000_9999sq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@c���q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1        @4      sq ~ 3pw    xq ~�t :owner_no__of_municipalities_with_inhabitants_greater_10000sq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�?�VǗ�I�q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1        @      sq ~ 3pw    xq ~�t owner_no__of_citiessq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@P���xq ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1?�      @&      sq ~ 3pw    xq ~�t  owner_ratio_of_urban_inhabitantssq ~    sq ~ sq ~ ?@     w      q ~ q ~wx sq ~  sq ~�@P�źHq ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1@@�3?�l�@Y      sq ~ 3pw    xq ~�t owner_average_salarysq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@�e��<��q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1@��     @�~�    sq ~ 3pw    xq ~�t owner_unemploymant_rate_96sq ~    sq ~ sq ~ ?@     w      q ~ q ~wx sq ~  sq ~�@��v#x�q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1?ۅ���@"�����sq ~ 3pw    xq ~�t /owner_no__of_enterpreneurs_per_1000_inhabitantssq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@]��kq ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1@T@     @d�     sq ~ 3pw    xq ~�t owner_no__of_commited_crimes_96sq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@�%���q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1@��     @�20    sq ~ 3pw    xq ~�t 	owner_sexsq ~    sq ~ sq ~ ?@     w      q ~ q ~ x sq ~  pq ~ 't femaleq ~sq ~ *q ~ .q ~ /q ~ psq ~ 1��      �      sq ~ 3pw   q ~	t malexq ~�t 	owner_agesq ~    sq ~ sq ~ ?@     w      q ~ t float8x sq ~  sq ~�@C�rB��q ~ /pq ~sq ~ *q ~ .q ~ /q ~ psq ~ 1@1      @N�     sq ~ 3pw    xq ~�t clients_countsq ~    sq ~ sq ~ ?@     w      q ~ t int8x sq ~  sq ~�?�)�f�tq ~ /pq ~sq ~ *q ~ .q ~ /q ~ psq ~ 1?�      @       sq ~ 3pw    xq ~�t 	card_typesq ~    sq ~ sq ~ ?@     w      q ~ q ~ x sq ~  pq ~ 't classicq ~"sq ~ *sq ~ ,  Iq ~ /q ~ psq ~ 1��      �      sq ~ 3pw   q ~'t goldt juniorxq ~�t has_credit_cardsq ~    sq ~ sq ~ ?@     w      q ~ t boolx sq ~  pq ~ 't fq ~.sq ~ *q ~ .q ~ /q ~ psq ~ 1��      �      sq ~ 3pw   q ~4t txq ~�t card_issued_owner_agesq ~    sq ~ sq ~ ?@     w      q ~ q ~x sq ~  sq ~�@CB�\(��q ~ /pq ~9sq ~ *sq ~ ,  Iq ~ /q ~ psq ~ 1@1      @M      sq ~ 3pw    xq ~�t card_issued_account_agesq ~    sq ~ sq ~ ?@     w      q ~ q ~x sq ~  sq ~�?���Q�q ~ /pq ~Csq ~ *sq ~ ,  Iq ~ /q ~ psq ~ 1        ?�      sq ~ 3pw    xq ~�t loan_to_card_issued_diffsq ~    sq ~ sq ~ ?@     w      q ~ q ~�x sq ~  sq ~�@e���Q�q ~ /pq ~Msq ~ *sq ~ ,  Iq ~ /q ~ psq ~ 1@      @}      sq ~ 3pw    xq ~�t avg_month_incomesq ~    sq ~ sq ~ ?@     w      q ~ q ~x sq ~  sq ~�@ڸ���l�q ~ /pq ~Wsq ~ *q ~ .q ~ /q ~ psq ~ 1@�     @�]rv'bvsq ~ 3pw    xq ~�t avg_month_interest_creditsq ~    sq ~ sq ~ ?@     w      q ~ q ~x sq ~  sq ~�@d#���zq ~ /pq ~`sq ~ *q ~ .q ~ /q ~ psq ~ 1        @w��   sq ~ 3pw    xq ~�t avg_month_widthdrawalsq ~    sq ~ sq ~ ?@     w      q ~ q ~x sq ~  sq ~�@�i�v��q ~ /pq ~isq ~ *q ~ .q ~ /q ~ psq ~ 1        @�Y��;�sq ~ 3pw    xq ~�t avg_month_trans_countsq ~    sq ~ sq ~ ?@     w      q ~ t numericx sq ~  sq ~�@G~$�e3q ~ /pq ~rsq ~ *q ~ .q ~ /q ~ psq ~ 1?�      @%      sq ~ 3pw    xq ~�t credit_instabilitysq ~    sq ~ sq ~ ?@     w      q ~ q ~x sq ~  sq ~�?��i��q ~ /pq ~|sq ~ *q ~ .q ~ /q ~ psq ~ 1?x0P��?�h�6��hsq ~ 3pw    xq ~�t max_balancesq ~    sq ~ sq ~ ?@     w      q ~ q ~wx sq ~  sq ~�@�ٕ�I��q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1@ú     A"�    sq ~ 3pw    xq ~�t has_had_negative_balancesq ~    sq ~ sq ~ ?@     w      q ~ q ~2x sq ~  pq ~ 't fq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1��      �      sq ~ 3pw   q ~�t txq ~�t balance_drop_ratiosq ~    sq ~ sq ~ ?@     w      q ~ q ~x sq ~  sq ~�@�ek�q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1?�������@=��v���sq ~ 3pw    xq ~�t has_remittancesq ~    sq ~ sq ~ ?@     w      q ~ q ~2x sq ~  pq ~ 't tq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1��      �      sq ~ 3pw   t fq ~�xq ~�t bank_connectionssq ~    sq ~ sq ~ ?@     w      q ~ q ~x sq ~  sq ~�?�$(F޼q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1        @      sq ~ 3pw    xq ~�t percentage_spentsq ~    sq ~ sq ~ ?@     w      q ~ q ~x sq ~  sq ~�?�2��Y1q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1        ?�8�wn�hsq ~ 3pw    xq ~�t salary_comparissonsq ~    sq ~ sq ~ ?@     w      q ~ q ~x sq ~  sq ~�@�fjd��q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1?�\�g@":!ԛuCsq ~ 3pw    xq ~�t shared_percentage_paymentsq ~    sq ~ sq ~ ?@     w      q ~ q ~x sq ~  sq ~�?�1UMq ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1?��Ӵ�,?��5���sq ~ 3pw    xq ~�t percentage_paymentsq ~    sq ~ sq ~ ?@     w      q ~ q ~x sq ~  sq ~�?��9`> q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1?�	��R@m?��5���sq ~ 3pw    xq ~�t percentage_from_max_balancesq ~    sq ~ sq ~ ?@     w      q ~ q ~wx sq ~  sq ~�?�\����q ~ /pq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1��l�ߊ��?�      sq ~ 3pw    xq ~�t bank_in_other_districtsq ~    sq ~ sq ~ ?@     w      q ~ q ~2x sq ~  pq ~ 't fq ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1��      �      sq ~ 3pw   q ~�xq ~�t statussq ~    sq ~ sq ~ ?@     w      q ~ q ~ x sq ~  pq ~ 't  q ~�sq ~ *q ~ .q ~ /q ~ psq ~ 1��      �      sq ~ 3pw   q ~�xq ~�x q ~�sq ~ *sq ~ ,  bq ~ /